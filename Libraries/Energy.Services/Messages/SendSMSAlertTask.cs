﻿using Energy.Core.Domain.Customers;
using Energy.Services.Bills;
using Energy.Services.Customers;
using Energy.Services.Logging;
using Energy.Services.Security;
using Energy.Services.Splits;
using Energy.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Energy.Services.Common;

namespace Energy.Services.Messages
{
    public partial class SendSMSAlertTask : ITask
    {
        private readonly ISplitService _splitService;
        private readonly IBillService _billService;
        private readonly ICustomerService _customerService;
        private readonly IPermissionService _permissionService;
        private readonly ISMSService _smsService;
        private readonly ILogger _logger;

        public SendSMSAlertTask(ISplitService splitService,
            IBillService billService,
            ICustomerService customerService,
            IPermissionService permissionService,
            ISMSService smsService,
            ILogger logger)
        {
            this._splitService = splitService;
            this._billService = billService;
            this._customerService = customerService;
            this._permissionService = permissionService;
            this._smsService = smsService;
            this._logger = logger;
        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public virtual void Execute()
        {
            var bills = _billService.GetBills();

            var lastYear = (from b in bills
                            select b.Year).Max();

            var lastTerm = (from b in bills
                            where b.Year == lastYear
                            select b.Term).Max();

            var lastBillExpDate = _billService.GetBillsByDate(lastYear, lastTerm).FirstOrDefault().ExpDate.HasValue ? _billService.GetBillsByDate(lastYear, lastTerm).FirstOrDefault().ExpDate.Value : (DateTime?)null;

            if (lastBillExpDate.HasValue)
            {
                try
                {
                    if (DateTime.Now.Day - lastBillExpDate.Value.Day == 1)
                    {
                        var admins = _customerService.GetAllCustomers(customerRoleIds: new int[] { 1 });
                        var msgText = "کاربر گرامی با توجه به اینکه یک روز از موعد پرداخت صورتحساب های دوره قبل گذشته است، لطفا گردش مالی مشترکین را با بخش مالی چک کنید";
                        foreach (var user in admins)
                        {
                            _smsService.SendSMS(user.GetAttribute<string>(SystemCustomerAttributeNames.Mobile), msgText);
                        }
                    }
                }
                catch(Exception exc)
                {
                    _logger.Error(string.Format("Error sending sms. {0}", exc.Message), exc);
                }
                finally { }
            }
        }
    }
}
